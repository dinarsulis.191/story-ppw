from django.urls import path
from . import views

app_name = 'morepage'

urlpatterns = [
    path('', views.story1, name='story1'),
]