from django.contrib import admin
from django.urls import path
from . import views
from .views import *

app_name = 'schedule'

urlpatterns = [
    path('schedule/', views.schedule_list, name='schedule' ),
    path('form-schedule/', views.form_schedule, name='form_schedule' ),
    path('detail-schedule/<int:id>', views.schedule_detail, name='detail_schedule'),
    path('remove-schedule/<int:id>', views.remove_schedule, name="remove_schedule"),
]