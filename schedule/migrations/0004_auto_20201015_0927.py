# Generated by Django 3.1.1 on 2020-10-15 02:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20201015_0924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='jumlahSks',
            field=models.IntegerField(default=2),
        ),
    ]
