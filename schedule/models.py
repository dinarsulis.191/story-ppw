from django.db import models

# Create your models here.

class Schedule(models.Model) :
    matkul = models.CharField(max_length=50,default='SOME STRING')
    dosen = models.CharField(max_length=40, default='SOME STRING')
    sks = models.IntegerField(default=1)
    deskripsi = models.TextField(max_length=100, default='SOME STRING')
    ruang = models.CharField(max_length=40,default='SOME STRING')

    GANJIL1 = "Ganjil 2019/2020"
    GENAP1 = "Genap 2019/2020"
    GANJIL2 = "Ganjil 2020/2021"
    GENAP2 = "Genap 2020/2021"
    tahunAjaran = [(GENAP1, 'Genap 2019/2020'), (GANJIL1, 'Ganjil 2019/2020'), 
        (GANJIL2, 'Ganjil 2020/2021'), (GENAP2, 'Genap 2020/2021')]
    tahun = models.CharField(max_length=16, choices=tahunAjaran, default="GENAP1")