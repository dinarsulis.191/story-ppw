from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm
# import datetime

# Create your views here.
def schedule_list(request, *args, **kwargs):
    sched_form = ScheduleForm(Schedule)
    schedules = Schedule.objects.all()

    context = {
        'form' : sched_form,
        'schedules' : schedules
    }
    return render(request, 'schedule.html', context)  

def schedule_detail(request, id):
    schedule = Schedule.objects.get(id=id)
    context = {
        'schedule': schedule
    }
    return render(request, 'schedule_detail.html', context)

def form_schedule(request):
    sched_form = ScheduleForm()

    if request.method == 'POST':
        sched_form = ScheduleForm(request.POST)
        if sched_form.is_valid():
            Schedule.objects.create(**sched_form.cleaned_data)
            return redirect('schedule:schedule')

    context = {
        'form' : sched_form,
    }
    return render(request, "form_schedule.html", context)  

def remove_schedule(request, id, *args, **kwargs):
    Schedule.objects.filter(id=id).delete()

    return redirect('schedule:schedule')
