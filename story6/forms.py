from django import forms
from .models import Activity, Member

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'
        widgets = {
            'activity' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Insert activity"}),
        }

class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = '__all__'
        widgets = {
            'member' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Insert name"}),
        }


