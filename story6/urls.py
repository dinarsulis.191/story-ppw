from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('activity/', views.activity, name='activity'),
    path('delete/<str:pk>', views.delete, name='activityDelete'),
    path('delete/member/<str:pk>', views.deleteMember, name='memberDelete')
]
